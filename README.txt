CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This module allows users to connect to their Playstation Network Account to
display their PSN profiles and trophies.


REQUIREMENTS
------------

* Playstation Network Account
* Composer



INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
Visit: https://www.drupal.org/docs/7/extend/installing-modules for
further information.

* Thirdparty libraries should be installed by composer



CONFIGURATION
-------------

* Enter your PSN credentials in Configuration » Web Services » PSN Public
Trophies Settings menu.
